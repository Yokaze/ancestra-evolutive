/*
 * ASTRUB INTEGRALE @ Ancestra_Remake
 */

/*7345*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '349' WHERE `houses`.`id` =703 LIMIT 1 ;

/*7331*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '139' WHERE `houses`.`id` =700 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `caseid` = '142' WHERE `houses`.`id` =700 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '144' WHERE `houses`.`id` =702 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '461' WHERE `houses`.`id` =701 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `mapid` = '7695', `caseid` = '150' WHERE `houses`.`id` =701 LIMIT 1 ;

/*7347*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '114' WHERE `houses`.`id` =699 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '300' WHERE `houses`.`id` =695 LIMIT 1 ;

/*7334*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '272' WHERE `houses`.`id` =706 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '283' WHERE `houses`.`id` =709 LIMIT 1 ;

/*7335*/
UPDATE `ancestra_other`.`houses` SET `caseid` = '402' WHERE `houses`.`id` =708 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '254' WHERE `houses`.`id` =707 LIMIT 1 ;

/*7352*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '129' WHERE `houses`.`id` =698 LIMIT 1 ;

/*7348*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '288' WHERE `houses`.`id` =697 LIMIT 1 ;

/*7360*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '223' WHERE `houses`.`id` =681 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '293' WHERE `houses`.`id` =682 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '173' WHERE `houses`.`id` =683 LIMIT 1 ;

/*7363*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '133' WHERE `houses`.`id` =684 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '216' WHERE `houses`.`id` =685 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '412' WHERE `houses`.`id` =686 LIMIT 1 ;

/*7381*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '236' WHERE `houses`.`id` =687 LIMIT 1 ;

/*7366*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '119' WHERE `houses`.`id` =694 LIMIT 1 ;

/*7367*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '456' WHERE `houses`.`id` =690 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '150' WHERE `houses`.`id` =691 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '187' WHERE `houses`.`id` =692 LIMIT 1 ;

/*7368*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '120' WHERE `houses`.`id` =693 LIMIT 1 ;

/*7384*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '165' WHERE `houses`.`id` =670 LIMIT 1 ;

/*7383*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '357' WHERE `houses`.`id` =671 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '187' WHERE `houses`.`id` =672 LIMIT 1 ;

/*7382*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '462' WHERE `houses`.`id` =696 LIMIT 1 ;

/*7364*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '158' WHERE `houses`.`id` =674 LIMIT 1 ;

/*7380*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '113' WHERE `houses`.`id` =675 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '122' WHERE `houses`.`id` =676 LIMIT 1 ;

/*7379*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '158' WHERE `houses`.`id` =677 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '178' WHERE `houses`.`id` =678 LIMIT 1 ;

/*7377*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '236' WHERE `houses`.`id` =679 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '140' WHERE `houses`.`id` =680 LIMIT 1 ;

/*7392*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '177' WHERE `houses`.`id` =660 LIMIT 1 ;

/*7289*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '204' WHERE `houses`.`id` =661 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '260' WHERE `houses`.`id` =662 LIMIT 1 ;

/*7394*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '299' WHERE `houses`.`id` =663 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '354' WHERE `houses`.`id` =664 LIMIT 1 ;

/*7397*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '158' WHERE `houses`.`id` =666 LIMIT 1 ;

/*7399*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '148' WHERE `houses`.`id` =667 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '141' WHERE `houses`.`id` =668 LIMIT 1 ;

/*7400*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '120' WHERE `houses`.`id` =669 LIMIT 1 ;

/*7415*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '162' WHERE `houses`.`id` =650 LIMIT 1 ;

/*7414*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '251' WHERE `houses`.`id` =771 LIMIT 1 ;

/*7413*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '89' WHERE `houses`.`id` =651 LIMIT 1 ;

/*7412*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '118' WHERE `houses`.`id` =653 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '170' WHERE `houses`.`id` =652 LIMIT 1 ;

/*7411*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '199' WHERE `houses`.`id` =654 LIMIT 1 ;

/*7410*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '309' WHERE `houses`.`id` =656 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '231' WHERE `houses`.`id` =655 LIMIT 1 ;

/*7408*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '223' WHERE `houses`.`id` =659 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '199' WHERE `houses`.`id` =658 LIMIT 1 ;

/*7425*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '199' WHERE `houses`.`id` =705 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '440' WHERE `houses`.`id` =704 LIMIT 1 ;

/*7426*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '148' WHERE `houses`.`id` =645 LIMIT 1 ;

/*7428*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '177' WHERE `houses`.`id` =647 LIMIT 1 ;

/*7430*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '428' WHERE `houses`.`id` =648 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '192' WHERE `houses`.`id` =649 LIMIT 1 ;

/*7447*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '133' WHERE `houses`.`id` =644 LIMIT 1 ;

/*7445*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '249' WHERE `houses`.`id` =642 LIMIT 1 ;
UPDATE `ancestra_other`.`houses` SET `cell_id` = '404' WHERE `houses`.`id` =643 LIMIT 1 ;

/*7444*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '178' WHERE `houses`.`id` =641 LIMIT 1 ;

/*7441*/
UPDATE `ancestra_other`.`houses` SET `cell_id` = '376' WHERE `houses`.`id` =640 LIMIT 1 ;