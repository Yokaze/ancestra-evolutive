package core;

import realm.RealmServer;

import common.SQLManager;
import common.World;

import enums.EmulatorInfos;
import game.GameServer;

public class Main {
	
	static {
		//reboot
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				Main.closeServers();
			}
		});
	}
	
	public static void main(String[] args) {
		//cr�ation de la console
		Console console = new Console();
		Console.instance = console;
		
		//d�marrage de l'�mualteur
		console.writeln(EmulatorInfos.SOFT_NAME.toString());
		console.writeln("\n ~ Initialisation des variables : config.conf");
		Server.config.initialize();
		console.writeln(" ~ Connexion a la base de donnees : "+Server.config.getHost());
		
		if(!SQLManager.setUpConnexion()) {
			console.writeln("> Identifiants de connexion invalides");
			console.writeln("> Redemarrage...");
			Main.closeServers();
			System.exit(0);
		}
		
		console.writeln(" > Creation du monde");
		int time = World.data.initialize();
		
		Server.config.setRunning(true);
		Server.config.setGameServer(new GameServer(Server.config.getIp()));
		Server.config.setRealmServer(new RealmServer());
		
		//serveur lanc�
		console.writeln(" > Lancement du serveur termine : "+ time +" secondes");
		console.writeln(" > HELP pour la liste de commandes");
		
		//lancement de la console
		console.initialize();
	}

	public static void closeServers() {
		Console.instance.writeln(" <> Fermeture du jeu <>");
		if(Server.config.isRunning()) {
			Server.config.setRunning(false);
			Server.config.getGameServer().kickAll();
			World.data.saveData(-1);
			SQLManager.closeCons();
		}
		Console.instance.writeln(" <> Redemmarage <>");
		Server.config.setRunning(false);
	}
}
