package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import objects.Action;
import objects.Carte;
import objects.Carte.MountPark;
import objects.Dragodinde;
import objects.Drop;
import objects.ExpLevel;
import objects.Guild;
import objects.Guild.GuildMember;
import objects.HDV;
import objects.HDV.HdvEntry;
import objects.House;
import objects.IOTemplate;
import objects.ItemSet;
import objects.Metier;
import objects.Monstre;
import objects.NPC_tmpl;
import objects.NPC_tmpl.NPC_question;
import objects.NPC_tmpl.NPC_reponse;
import objects.Objet;
import objects.Objet.ObjTemplate;
import objects.Percepteur;
import objects.Personnage;
import objects.Sort;
import objects.Sort.SortStats;
import objects.Trunk;

import com.mysql.jdbc.PreparedStatement;

import core.Console;
import core.Log;
import core.Main;
import core.Server;

public class SQLManager {

	private static Connection othCon;
	private static Connection statCon;

	private static Timer timerCommit;
	private static boolean needCommit;

	public synchronized static ResultSet executeQuery(String query,
			String DBNAME) throws SQLException {
		Connection DB;
		if (DBNAME.equals(Server.config.getOtherDatabaseName()))
			DB = othCon;
		else
			DB = statCon;

		Statement stat = DB.createStatement();
		ResultSet RS = stat.executeQuery(query);
		stat.setQueryTimeout(300);
		return RS;
	}

	public synchronized static PreparedStatement newTransact(String baseQuery,
			Connection dbCon) throws SQLException {
		PreparedStatement toReturn = (PreparedStatement) dbCon
				.prepareStatement(baseQuery);

		needCommit = true;
		return toReturn;
	}

	public synchronized static void commitTransacts() {
		try {
			if (othCon.isClosed() || statCon.isClosed()) {
				closeCons();
				setUpConnexion();
			}

			statCon.commit();
			othCon.commit();
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR:" + e.getMessage());
			e.printStackTrace();
			commitTransacts();
		}
	}

	public synchronized static void closeCons() {
		try {
			commitTransacts();

			othCon.close();
			statCon.close();
		} catch (Exception e) {
			Console.instance
					.println("Erreur a la fermeture des connexions SQL:"
							+ e.getMessage());
			e.printStackTrace();
		}
	}

	public static final boolean setUpConnexion() {
		try {
			othCon = DriverManager.getConnection(
					"jdbc:mysql://" + Server.config.getHost() + "/"
							+ Server.config.getOtherDatabaseName(),
					Server.config.getUser(), Server.config.getPass());
			othCon.setAutoCommit(false);

			statCon = DriverManager.getConnection(
					"jdbc:mysql://" + Server.config.getHost() + "/"
							+ Server.config.getStaticDatabaseName(),
					Server.config.getUser(), Server.config.getPass());
			statCon.setAutoCommit(false);

			if (!statCon.isValid(1000) || !othCon.isValid(1000)) {
				Log.addToLog("SQLError : Connexion a la BD invalide!");
				return false;
			}

			needCommit = false;
			TIMER(true);

			return true;
		} catch (SQLException e) {
			Console.instance.println("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	private static void closeResultSet(ResultSet RS) {
		try {
			RS.getStatement().close();
			RS.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	private static void closePreparedStatement(PreparedStatement p) {
		try {
			p.clearParameters();
			p.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void LOAD_CRAFTS() {
		try {
			ResultSet RS = SQLManager.executeQuery("SELECT * from crafts;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				ArrayList<Couple<Integer, Integer>> m = new ArrayList<Couple<Integer, Integer>>();

				boolean cont = true;
				for (String str : RS.getString("craft").split(";")) {
					try {
						int tID = Integer.parseInt(str.split("\\*")[0]);
						int qua = Integer.parseInt(str.split("\\*")[1]);
						m.add(new Couple<Integer, Integer>(tID, qua));
					} catch (Exception e) {
						e.printStackTrace();
						cont = false;
					}
					;
				}
				// s'il y a eu une erreur de parsing, on ignore cette recette
				if (!cont)
					continue;

				World.data.addCraft(RS.getInt("id"), m);
			}
			closeResultSet(RS);
			;
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void LOAD_DROPS() {
		try {
			ResultSet RS = SQLManager.executeQuery("SELECT * from drops;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				Monstre MT = World.data.getMonstre(RS.getInt("mob"));
				MT.addDrop(new Drop(RS.getInt("item"), RS.getInt("seuil"), RS
						.getFloat("taux"), RS.getInt("max")));
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void LOAD_IOTEMPLATE() {
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT * from interactive_objects_data;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				World.data.addIOTemplate(new IOTemplate(RS.getInt("id"), RS
						.getInt("respawn"), RS.getInt("duration"), RS
						.getInt("unknow"), RS.getInt("walkable") == 1));
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static int LOAD_NPCS() {
		int nbr = 0;
		try {
			ResultSet RS = SQLManager.executeQuery("SELECT * from npcs;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				Carte map = World.data.getCarte(RS.getShort("mapid"));
				if (map == null)
					continue;
				map.addNpc(RS.getInt("npcid"), RS.getInt("cellid"),
						RS.getInt("orientation"));

				nbr++;
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
			nbr = 0;
		}
		return nbr;
	}

	public static void LOAD_EXP() {
		try {
			ResultSet RS = SQLManager.executeQuery("SELECT * from experience;",
					Server.config.getStaticDatabaseName());
			while (RS.next())
				World.data.addExpLevel(RS.getInt("lvl"),
						new ExpLevel(RS.getLong("perso"), RS.getInt("metier"),
								RS.getInt("dinde"), RS.getInt("pvp")));
			closeResultSet(RS);
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
	}

	public static int LOAD_TRIGGERS() {
		try {
			int nbr = 0;
			ResultSet RS = SQLManager.executeQuery(
					"SELECT * FROM `scripted_cells`",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				if (World.data.getCarte(RS.getShort("MapID")) == null)
					continue;
				if (World.data.getCarte(RS.getShort("MapID")).getCase(
						RS.getInt("CellID")) == null)
					continue;

				switch (RS.getInt("EventID")) {
				case 1:// Stop sur la case(triggers)
					World.data
							.getCarte(RS.getShort("MapID"))
							.getCase(RS.getInt("CellID"))
							.addOnCellStopAction(RS.getInt("ActionID"),
									RS.getString("ActionsArgs"),
									RS.getString("Conditions"));
					break;

				default:
					Log.addToLog("Action Event " + RS.getInt("EventID")
							+ " non implante");
					break;
				}
				nbr++;
			}
			closeResultSet(RS);
			return nbr;
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
		return 0;
	}
	public static void LOAD_SORTS() {
		try {
			ResultSet RS = SQLManager.executeQuery("SELECT  * from sorts;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				int id = RS.getInt("id");
				Sort sort = new Sort(id, RS.getInt("sprite"),
						RS.getString("spriteInfos"),
						RS.getString("effectTarget"));
				SortStats l1 = parseSortStats(id, 1, RS.getString("lvl1"));
				SortStats l2 = parseSortStats(id, 2, RS.getString("lvl2"));
				SortStats l3 = parseSortStats(id, 3, RS.getString("lvl3"));
				SortStats l4 = parseSortStats(id, 4, RS.getString("lvl4"));
				SortStats l5 = null;
				if (!RS.getString("lvl5").equalsIgnoreCase("-1"))
					l5 = parseSortStats(id, 5, RS.getString("lvl5"));
				SortStats l6 = null;
				if (!RS.getString("lvl6").equalsIgnoreCase("-1"))
					l6 = parseSortStats(id, 6, RS.getString("lvl6"));
				sort.addSortStats(1, l1);
				sort.addSortStats(2, l2);
				sort.addSortStats(3, l3);
				sort.addSortStats(4, l4);
				sort.addSortStats(5, l5);
				sort.addSortStats(6, l6);
				World.data.addSort(sort);
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
	}

	private static SortStats parseSortStats(int id, int lvl, String str) {
		try {
			SortStats stats = null;
			String[] stat = str.split(",");
			String effets = stat[0];
			String CCeffets = stat[1];
			int PACOST = 6;
			try {
				PACOST = Integer.parseInt(stat[2].trim());
			} catch (NumberFormatException e) {
			}
			;

			int POm = Integer.parseInt(stat[3].trim());
			int POM = Integer.parseInt(stat[4].trim());
			int TCC = Integer.parseInt(stat[5].trim());
			int TEC = Integer.parseInt(stat[6].trim());
			boolean line = stat[7].trim().equalsIgnoreCase("true");
			boolean LDV = stat[8].trim().equalsIgnoreCase("true");
			boolean emptyCell = stat[9].trim().equalsIgnoreCase("true");
			boolean MODPO = stat[10].trim().equalsIgnoreCase("true");
			// int unk = Integer.parseInt(stat[11]);//All 0
			int MaxByTurn = Integer.parseInt(stat[12].trim());
			int MaxByTarget = Integer.parseInt(stat[13].trim());
			int CoolDown = Integer.parseInt(stat[14].trim());
			String type = stat[15].trim();
			int level = Integer.parseInt(stat[stat.length - 2].trim());
			boolean endTurn = stat[19].trim().equalsIgnoreCase("true");
			stats = new SortStats(id, lvl, PACOST, POm, POM, TCC, TEC, line,
					LDV, emptyCell, MODPO, MaxByTurn, MaxByTarget, CoolDown,
					level, endTurn, effets, CCeffets, type);
			return stats;
		} catch (Exception e) {
			e.printStackTrace();
			int nbr = 0;
			Console.instance.println("[DEBUG]Sort " + id + " lvl " + lvl);
			for (String z : str.split(",")) {
				Console.instance.println("[DEBUG]" + nbr + " " + z);
				nbr++;
			}
			System.exit(1);
			return null;
		}
	}

	public static void LOAD_NPC_TEMPLATE() {
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT * FROM npc_template;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				int id = RS.getInt("id");
				int bonusValue = RS.getInt("bonusValue");
				int gfxID = RS.getInt("gfxID");
				int scaleX = RS.getInt("scaleX");
				int scaleY = RS.getInt("scaleY");
				int sex = RS.getInt("sex");
				int color1 = RS.getInt("color1");
				int color2 = RS.getInt("color2");
				int color3 = RS.getInt("color3");
				String access = RS.getString("accessories");
				int extraClip = RS.getInt("extraClip");
				int customArtWork = RS.getInt("customArtWork");
				int initQId = RS.getInt("initQuestion");
				String ventes = RS.getString("ventes");
				World.data.addNpcTemplate(new NPC_tmpl(id, bonusValue, gfxID,
						scaleX, scaleY, sex, color1, color2, color3, access,
						extraClip, customArtWork, initQId, ventes));
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
	}

	public static boolean SAVE_NEW_FIXGROUP(int mapID, int cellID,
			String groupData) {
		try {
			String baseQuery = "REPLACE INTO `mobgroups_fix` VALUES(?,?,?)";
			PreparedStatement p = newTransact(baseQuery, statCon);

			p.setInt(1, mapID);
			p.setInt(2, cellID);
			p.setString(3, groupData);

			p.execute();
			closePreparedStatement(p);

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void LOAD_NPC_QUESTIONS() {
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT * FROM npc_questions;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				World.data.addNPCQuestion(new NPC_question(RS.getInt("ID"), RS
						.getString("responses"), RS.getString("params"), RS
						.getString("cond"), RS.getInt("ifFalse")));
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
	}

	public static void LOAD_NPC_ANSWERS() {
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT * FROM npc_reponses_actions;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				int id = RS.getInt("ID");
				int type = RS.getInt("type");
				String args = RS.getString("args");
				if (World.data.getNPCreponse(id) == null)
					World.data.addNPCreponse(new NPC_reponse(id));
				World.data.getNPCreponse(id).addAction(
						new Action(type, args, ""));

			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
	}

	public static int LOAD_ENDFIGHT_ACTIONS() {
		int nbr = 0;
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT * FROM endfight_action;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				Carte map = World.data.getCarte(RS.getShort("map"));
				if (map == null)
					continue;
				map.addEndFightAction(RS.getInt("fighttype"),
						new Action(RS.getInt("action"), RS.getString("args"),
								RS.getString("cond")));
				nbr++;
			}
			closeResultSet(RS);
			return nbr;
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
		return nbr;
	}

	public static int LOAD_ITEM_ACTIONS() {
		int nbr = 0;
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT * FROM use_item_actions;",
					Server.config.getStaticDatabaseName());
			while (RS.next()) {
				int id = RS.getInt("template");
				int type = RS.getInt("type");
				String args = RS.getString("args");
				if (World.data.getObjTemplate(id) == null)
					continue;
				World.data.getObjTemplate(id).addAction(
						new Action(type, args, ""));
				nbr++;
			}
			closeResultSet(RS);
			return nbr;
		} catch (SQLException e) {
			Console.instance.println("Game: SQL ERROR: " + e.getMessage());
			System.exit(1);
		}
		return nbr;
	}

	public static boolean SAVE_TRIGGER(int mapID1, int cellID1, int action,
			int event, String args, String cond) {
		String baseQuery = "REPLACE INTO `scripted_cells`"
				+ " VALUES (?,?,?,?,?,?);";

		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setInt(1, mapID1);
			p.setInt(2, cellID1);
			p.setInt(3, action);
			p.setInt(4, event);
			p.setString(5, args);
			p.setString(6, cond);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}

	public static boolean REMOVE_TRIGGER(int mapID, int cellID) {
		String baseQuery = "DELETE FROM `scripted_cells` WHERE "
				+ "`MapID` = ? AND " + "`CellID` = ?;";
		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setInt(1, mapID);
			p.setInt(2, cellID);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}
	public static boolean DELETE_NPC_ON_MAP(int m, int c) {
		String baseQuery = "DELETE FROM npcs WHERE mapid = ? AND cellid = ?;";
		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setInt(1, m);
			p.setInt(2, c);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}

	public static boolean ADD_NPC_ON_MAP(int m, int id, int c, int o) {

		String baseQuery = "INSERT INTO `npcs`" + " VALUES (?,?,?,?);";
		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setInt(1, m);
			p.setInt(2, id);
			p.setInt(3, c);
			p.setInt(4, o);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}
	public static boolean ADD_ENDFIGHTACTION(int mapID, int type, int Aid,
			String args, String cond) {
		if (!DEL_ENDFIGHTACTION(mapID, type, Aid))
			return false;
		String baseQuery = "INSERT INTO `endfight_action` "
				+ "VALUES (?,?,?,?,?);";
		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setInt(1, mapID);
			p.setInt(2, type);
			p.setInt(3, Aid);
			p.setString(4, args);
			p.setString(5, cond);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}

	public static boolean DEL_ENDFIGHTACTION(int mapID, int type, int aid) {
		String baseQuery = "DELETE FROM `endfight_action` "
				+ "WHERE map = ? AND " + "fighttype = ? AND " + "action = ?;";
		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setInt(1, mapID);
			p.setInt(2, type);
			p.setInt(3, aid);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
			return false;
		}
	}

	public static boolean ADD_REPONSEACTION(int repID, int type, String args) {
		String baseQuery = "DELETE FROM `npc_reponses_actions` "
				+ "WHERE `ID` = ? AND " + "`type` = ?;";
		PreparedStatement p;
		try {
			p = newTransact(baseQuery, statCon);
			p.setInt(1, repID);
			p.setInt(2, type);

			p.execute();
			closePreparedStatement(p);
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		baseQuery = "INSERT INTO `npc_reponses_actions` " + "VALUES (?,?,?);";
		try {
			p = newTransact(baseQuery, statCon);
			p.setInt(1, repID);
			p.setInt(2, type);
			p.setString(3, args);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}

	public static boolean UPDATE_INITQUESTION(int id, int q) {
		String baseQuery = "UPDATE `npc_template` SET " + "`initQuestion` = ? "
				+ "WHERE `id` = ?;";
		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setInt(1, q);
			p.setInt(2, id);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}

	public static boolean UPDATE_NPCREPONSES(int id, String reps) {
		String baseQuery = "UPDATE `npc_questions` SET " + "`responses` = ? "
				+ "WHERE `ID` = ?;";
		try {
			PreparedStatement p = newTransact(baseQuery, statCon);
			p.setString(1, reps);
			p.setInt(2, id);

			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}

	public static void LOAD_ACTION() {
		/* Variables repr�sentant les champs de la base */
		Personnage perso;
		int action;
		int nombre;
		int id;
		Log.addToShopLog("Lancement de l'application des Lives Actions ...");
		String sortie;
		String couleur = "DF0101"; // La couleur du message envoyer a
									// l'utilisateur (couleur en code HTML)
		ObjTemplate t;
		Objet obj;
		PreparedStatement p;
		/* FIN */
		try {
			ResultSet RS = executeQuery("SELECT * from live_action;",
					Server.config.getOtherDatabaseName());

			while (RS.next()) {
				perso = World.data.getPersonnage(RS.getInt("PlayerID"));
				if (perso == null) {
					Log.addToShopLog("Personnage " + RS.getInt("PlayerID")
							+ " non trouve, personnage non charge ?");
					continue;
				}
				if (!perso.isOnline()) {
					Log.addToShopLog("Personnage " + RS.getInt("PlayerID")
							+ " hors ligne");
					continue;
				}
				if (perso.get_compte() == null) {
					Log.addToShopLog("Le Personnage " + RS.getInt("PlayerID")
							+ " n'est attribue a aucun compte charge");
					continue;
				}
				if (perso.get_compte().getGameThread() == null) {
					Log.addToShopLog("Le Personnage "
							+ RS.getInt("PlayerID")
							+ " n'a pas thread associe, le personnage est il hors ligne ?");
					continue;
				}
				if (perso.get_fight() != null)
					continue; // Perso en combat @ Nami-Doc
				action = RS.getInt("Action");
				nombre = RS.getInt("Nombre");
				id = RS.getInt("ID");
				sortie = "+";

				switch (action) {
				case 1: // Monter d'un level
					if (perso.get_lvl() == World.data.getExpLevelSize())
						continue;
					for (int n = nombre; n > 1; n--)
						perso.levelUp(false, true);
					perso.levelUp(true, true);
					sortie += nombre + " Niveau(x)";
					break;
				case 2: // Ajouter X point d'experience
					if (perso.get_lvl() == World.data.getExpLevelSize())
						continue;
					perso.addXp(nombre);
					sortie += nombre + " Xp";
					break;
				case 3: // Ajouter X kamas
					perso.addKamas(nombre);
					sortie += nombre + " Kamas";
					break;
				case 4: // Ajouter X point de capital
					perso.addCapital(nombre);
					sortie += nombre + " Point(s) de capital";
					break;
				case 5: // Ajouter X point de sort
					perso.addSpellPoint(nombre);
					sortie += nombre + " Point(s) de sort";
					break;
				case 20: // Ajouter un item avec des jets al�atoire
					t = World.data.getObjTemplate(nombre);
					if (t == null)
						continue;
					obj = t.createNewItem(1, false); // Si mis � "true" l'objet
														// � des jets max. Sinon
														// ce sont des jets
														// al�atoire
					if (obj == null)
						continue;
					if (perso.addObjet(obj, true))// Si le joueur n'avait pas
													// d'item similaire
						World.data.addObjet(obj, true);
					Log.addToSockLog("Objet " + nombre + " ajouter a "
							+ perso.get_name() + " avec des stats aleatoire");
					SocketManager
							.GAME_SEND_MESSAGE(
									perso,
									"L'objet \""
											+ t.getName()
											+ "\" viens d'etre ajouter a votre personnage",
									couleur);
					break;
				case 21: // Ajouter un item avec des jets MAX
					t = World.data.getObjTemplate(nombre);
					if (t == null)
						continue;
					obj = t.createNewItem(1, true); // Si mis � "true" l'objet �
													// des jets max. Sinon ce
													// sont des jets al�atoire
					if (obj == null)
						continue;
					if (perso.addObjet(obj, true))// Si le joueur n'avait pas
													// d'item similaire
						World.data.addObjet(obj, true);
					Log.addToSockLog("Objet " + nombre + " ajoute a "
							+ perso.get_name() + " avec des stats MAX");
					SocketManager
							.GAME_SEND_MESSAGE(
									perso,
									"L'objet \""
											+ t.getName()
											+ "\" avec des stats maximum, viens d'etre ajoute a votre personnage",
									couleur);
					break;
				case 118:// Force
					perso.get_baseStats().addOneStat(action, nombre);
					SocketManager.GAME_SEND_STATS_PACKET(perso);
					sortie += nombre + " force";
					break;
				case 119:// Agilite
					perso.get_baseStats().addOneStat(action, nombre);
					SocketManager.GAME_SEND_STATS_PACKET(perso);
					sortie += nombre + " agilite";
					break;
				case 123:// Chance
					perso.get_baseStats().addOneStat(action, nombre);
					SocketManager.GAME_SEND_STATS_PACKET(perso);
					sortie += nombre + " chance";
					break;
				case 124:// Sagesse
					perso.get_baseStats().addOneStat(action, nombre);
					SocketManager.GAME_SEND_STATS_PACKET(perso);
					sortie += nombre + " sagesse";
					break;
				case 125:// Vita
					perso.get_baseStats().addOneStat(action, nombre);
					SocketManager.GAME_SEND_STATS_PACKET(perso);
					sortie += nombre + " vita";
					break;
				case 126:// Intelligence
					int statID = action;
					perso.get_baseStats().addOneStat(statID, nombre);
					SocketManager.GAME_SEND_STATS_PACKET(perso);
					sortie += nombre + " intelligence";
					break;
				}
				SocketManager.GAME_SEND_STATS_PACKET(perso);
				if (action < 20 || action > 100)
					SocketManager.GAME_SEND_MESSAGE(perso, sortie
							+ " a votre personnage", couleur); // Si l'action
																// n'est pas un
																// ajout d'objet
																// on envoye un
																// message a
																// l'utilisateur

				Log.addToShopLog("(Commande " + id + ")Action " + action
						+ " Nombre: " + nombre
						+ " appliquee sur le personnage "
						+ RS.getInt("PlayerID") + "(" + perso.get_name() + ")");
				try {
					String query = "DELETE FROM live_action WHERE ID=" + id
							+ ";";
					p = newTransact(query, othCon);
					p.execute();
					closePreparedStatement(p);
					Log.addToShopLog("Commande " + id + " supprimee.");
				} catch (SQLException e) {
					Log.addToLog("SQL ERROR: " + e.getMessage());
					Log.addToShopLog("Error Delete From: " + e.getMessage());
					e.printStackTrace();
				}
				SQLManager.SAVE_PERSONNAGE(perso, true);
			}
			closeResultSet(RS);
		} catch (Exception e) {
			Log.addToLog("ERROR: " + e.getMessage());
			Log.addToShopLog("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void TIMER(boolean start) {
		if (start) {
			timerCommit = new Timer();
			timerCommit.schedule(new TimerTask() {

				public void run() {
					if (!needCommit)
						return;

					commitTransacts();
					needCommit = false;

				}
			}, Server.config.getDbCommit(), Server.config.getDbCommit());
		} else
			timerCommit.cancel();
	}

	public static int LOAD_ZAAPIS() {
		int i = 0;
		String Bonta = "";
		String Brak = "";
		String Neutre = "";
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT mapid, align from zaapi;",
					Server.config.getOtherDatabaseName());
			while (RS.next()) {
				if (RS.getInt("align") == Constants.ALIGNEMENT_BONTARIEN) {
					Bonta += RS.getString("mapid");
					if (!RS.isLast())
						Bonta += ",";
				} else if (RS.getInt("align") == Constants.ALIGNEMENT_BRAKMARIEN) {
					Brak += RS.getString("mapid");
					if (!RS.isLast())
						Brak += ",";
				} else {
					Neutre += RS.getString("mapid");
					if (!RS.isLast())
						Neutre += ",";
				}
				i++;
			}
			Constants.ZAAPI.put(Constants.ALIGNEMENT_BONTARIEN, Bonta);
			Constants.ZAAPI.put(Constants.ALIGNEMENT_BRAKMARIEN, Brak);
			Constants.ZAAPI.put(Constants.ALIGNEMENT_NEUTRE, Neutre);
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
		return i;
	}

	public static int LOAD_ZAAPS() {
		int i = 0;
		try {
			ResultSet RS = SQLManager.executeQuery(
					"SELECT mapID, cellID from zaaps;",
					Server.config.getOtherDatabaseName());
			while (RS.next()) {
				Constants.ZAAPS.put(RS.getInt("mapID"), RS.getInt("cellID"));
				i++;
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
		return i;
	}

	public static int LOAD_BANIP() {
		int i = 0;
		try {
			ResultSet RS = executeQuery("SELECT ip from banip;",
					Server.config.getOtherDatabaseName());
			while (RS.next()) {
				Constants.BAN_IP += RS.getString("ip");
				if (!RS.isLast())
					Constants.BAN_IP += ",";
				i++;
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
		return i;
	}

	public static boolean ADD_BANIP(String ip) {
		String baseQuery = "INSERT INTO `banip`" + " VALUES (?);";
		try {
			PreparedStatement p = newTransact(baseQuery, othCon);
			p.setString(1, ip);
			p.execute();
			closePreparedStatement(p);
			return true;
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + baseQuery);
		}
		return false;
	}

	public static void LOAD_HDVS() {
		try {
			ResultSet RS = executeQuery("SELECT * FROM `hdvs` ORDER BY id ASC",
					Server.config.getOtherDatabaseName());

			while (RS.next()) {
				World.data.addHdv(new HDV(RS.getInt("map"), RS
						.getFloat("sellTaxe"), RS.getShort("sellTime"), RS
						.getShort("accountItem"), RS.getShort("lvlMax"), RS
						.getString("categories")));

			}

			RS = executeQuery("SELECT id MAX FROM `hdvs`",
					Server.config.getOtherDatabaseName());
			RS.first();
			World.data.setNextHdvID(RS.getInt("MAX"));

			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void LOAD_HDVS_ITEMS() {
		try {
			long time1 = System.currentTimeMillis(); // TIME
			ResultSet RS = executeQuery("SELECT i.*"
					+ " FROM `items` AS i,`hdvs_items` AS h"
					+ " WHERE i.guid = h.itemID",
					Server.config.getOtherDatabaseName());

			// Load items
			while (RS.next()) {
				int guid = RS.getInt("guid");
				int tempID = RS.getInt("template");
				int qua = RS.getInt("qua");
				int pos = RS.getInt("pos");
				String stats = RS.getString("stats");
				World.data.addObjet(
						World.data.newObjet(guid, tempID, qua, pos, stats),
						false);
			}

			// Load HDV entry
			RS = executeQuery("SELECT * FROM `hdvs_items`",
					Server.config.getOtherDatabaseName());
			while (RS.next()) {
				HDV tempHdv = World.data.getHdv(RS.getInt("map"));
				if (tempHdv == null)
					continue;

				tempHdv.addEntry(new HDV.HdvEntry(RS.getInt("price"), RS
						.getByte("count"), RS.getInt("ownerGuid"), World.data
						.getObjet(RS.getInt("itemID"))));
			}
			Console.instance.println(System.currentTimeMillis() - time1
					+ "ms pour loader les HDVS items"); // TIME
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static void SAVE_HDVS_ITEMS(ArrayList<HdvEntry> liste) {
		PreparedStatement queries = null;
		try {
			String emptyQuery = "TRUNCATE TABLE `hdvs_items`";
			PreparedStatement emptyTable = newTransact(emptyQuery, othCon);
			emptyTable.execute();
			closePreparedStatement(emptyTable);

			String baseQuery = "INSERT INTO `hdvs_items` "
					+ "(`map`,`ownerGuid`,`price`,`count`,`itemID`) "
					+ "VALUES(?,?,?,?,?);";
			queries = newTransact(baseQuery, othCon);
			for (HdvEntry curEntry : liste) {

				if (curEntry.getOwner() == -1)
					continue;
				queries.setInt(1, curEntry.getHdvID());
				queries.setInt(2, curEntry.getOwner());
				queries.setInt(3, curEntry.getPrice());
				queries.setInt(4, curEntry.getAmount(false));
				queries.setInt(5, curEntry.getObjet().getGuid());

				queries.execute();
			}
			closePreparedStatement(queries);
			SAVE_HDV_AVGPRICE();
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public static int LOAD_TRUNK() {
		int nbr = 0;
		try {
			ResultSet RS = SQLManager.executeQuery("SELECT * from coffres;",
					Server.config.getOtherDatabaseName());
			while (RS.next()) {
				World.data.addTrunk(new Trunk(RS.getInt("id"), RS
						.getInt("id_house"), RS.getShort("mapid"), RS
						.getInt("cellid"), RS.getString("object"), RS
						.getInt("kamas"), RS.getString("key"), RS
						.getInt("owner_id")));
				nbr++;
			}
			closeResultSet(RS);
		} catch (SQLException e) {
			Log.addToLog("SQL ERROR: " + e.getMessage());
			e.printStackTrace();
			nbr = 0;
		}
		return nbr;
	}

	public static void TRUNK_CODE(Personnage P, Trunk t, String packet) {
		PreparedStatement p;
		String query = "UPDATE `coffres` SET `key`=? WHERE `id`=? AND owner_id=?;";
		try {
			p = newTransact(query, othCon);
			p.setString(1, packet);
			p.setInt(2, t.get_id());
			p.setInt(3, P.getAccID());

			p.execute();
			closePreparedStatement(p);
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + query);
		}
	}

	public static void UPDATE_TRUNK(Trunk t) {
		PreparedStatement p;
		String query = "UPDATE `coffres` SET `kamas`=?, `object`=? WHERE `id`=?";

		try {
			p = newTransact(query, othCon);
			p.setLong(1, t.get_kamas());
			p.setString(2, t.parseTrunkObjetsToDB());
			p.setInt(3, t.get_id());

			p.execute();
			closePreparedStatement(p);
		} catch (SQLException e) {
			Log.addToLog("Game: SQL ERROR: " + e.getMessage());
			Log.addToLog("Game: Query: " + query);
		}
	}
}
