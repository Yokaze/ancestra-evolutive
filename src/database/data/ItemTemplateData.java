package database.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import objects.Objet.ObjTemplate;

import common.World;

import core.Console;
import database.AbstractDAO;

public class ItemTemplateData extends AbstractDAO<ObjTemplate>{

	public ItemTemplateData(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(ObjTemplate obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(ObjTemplate obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(ObjTemplate obj) {
		try {
			String baseQuery = "UPDATE `item_template`"
					+ " SET sold = ?, avgPrice = ?" + " WHERE id = ?";
			PreparedStatement statement = connection.prepareStatement(baseQuery);

			statement.setLong(1, obj.getSold());
			statement.setInt(2, obj.getAvgPrice());
			statement.setInt(3, obj.getID());
			
			execute(statement);
			
			return true;
		} catch (SQLException e) {
			Console.instance.writeln("SQL ERROR: "+e.getMessage());
		}
		return false;
	}

	@Override
	public ObjTemplate load(int id) {
		ObjTemplate template = null;
		try {
			ResultSet result = getData("SELECT FROM item_template WHERE id = "+id);
			
			if(result.next()) {
				template = new ObjTemplate(result.getInt("id"), result
						.getString("statsTemplate"), result.getString("name"), result
						.getInt("type"), result.getInt("level"), result.getInt("pod"),
						result.getInt("prix"), result.getInt("panoplie"), result
								.getString("condition"), result
								.getString("armesInfos"), result.getInt("sold"), result
								.getInt("avgPrice"));
				World.data.addObjTemplate(template);
			}
			closeResultSet(result);
		} catch (SQLException e) {
			Console.instance.writeln("SQL ERROR: "+e.getMessage());
		}
		return template;
	}
}
