package database.data;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import objects.Metier;

import common.World;

import core.Console;
import database.AbstractDAO;

public class JobData extends AbstractDAO<Metier>{

	public JobData(Connection connection) {
		super(connection);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean create(Metier obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Metier obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(Metier obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Metier load(int id) {
		Metier job = null;
		try {
			ResultSet result = getData("SELECT FROM jobs_data WHERE id = "+id);
			
			if(result.next()) {
				job = new Metier(result.getInt("id"), result
						.getString("tools"), result.getString("crafts"));
				World.data.addJob(job);
			}
			closeResultSet(result);
		} catch (SQLException e) {
			Console.instance.writeln("SQL ERROR: "+e.getMessage());
		}
		return job;
	}
}
