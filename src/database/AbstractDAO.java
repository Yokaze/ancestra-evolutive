package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import core.Console;

public abstract class AbstractDAO<T> implements DAO<T>{
	protected final Connection connection;
	  
	public AbstractDAO(Connection connection) {
		this.connection = connection;
	}
	
	protected void execute(String query) {
		//on stop l'auto commit
	    try {
			connection.setAutoCommit(false);
		} catch (SQLException e1) { 
			Console.instance.writeln(" > SQL ERROR: "+e1.getMessage());
		}
	    
	    //on execute la query
	    try {
	        ResultSet result = connection.createStatement()
	                                     .executeQuery(query);
	        closeResultSet(result);
	        connection.commit();
	    } catch (Exception e) {
	        try {
				connection.rollback();
			} catch (SQLException e1) {
				Console.instance.writeln(" > SQL ERROR: "+e1.getMessage()); 
			}
	    } finally {
	    	//on relance l'auto commit
	        try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				Console.instance.writeln(" > SQL ERROR: "+e.getMessage());
			}
	    }
	}
	
	protected void execute(PreparedStatement statement) {
		//on stop l'auto commit
	    try {
			connection.setAutoCommit(false);
		} catch (SQLException e1) { 
			Console.instance.writeln(" > SQL ERROR: "+e1.getMessage());
		}
	    
	    //on execute la query
	    try {
	        statement.execute();
	        closeStatement(statement);
	        connection.commit();
	    } catch (Exception e) {
	        try {
				connection.rollback();
			} catch (SQLException e1) {
				Console.instance.writeln(" > SQL ERROR: "+e1.getMessage()); 
			}
	    } finally {
	    	//on relance l'auto commit
	        try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				Console.instance.writeln(" > SQL ERROR: "+e.getMessage());
			}
	    }
	}
	
	protected ResultSet getData(String query) {
		//on stop l'auto commit
	    try {
			connection.setAutoCommit(false);
		} catch (SQLException e1) { 
			Console.instance.writeln(" > SQL ERROR: "+e1.getMessage());
		}
	    
	    //on execute la query
	    try {
	        ResultSet result = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)
	                                     .executeQuery(query);
	        connection.commit();
	        return result;
	    } catch (Exception e) {
	        try {
				connection.rollback();
			} catch (SQLException e1) {
				Console.instance.writeln(" > SQL ERROR: "+e1.getMessage()); 
			}
	        return null;
	    } finally {
	    	//on relance l'auto commit
	        try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				Console.instance.writeln(" > SQL ERROR: "+e.getMessage());
			}
	    }
	}
	
	protected void closeResultSet(ResultSet result) {
		try {
			result.close();
			result.getStatement().close();
		} catch (SQLException e) {
			Console.instance.writeln(e.getMessage());
		}
	}
	
	protected void closeStatement(PreparedStatement statement) {
		try {
			statement.clearParameters();
	        statement.close();
		} catch (SQLException e) {
			Console.instance.writeln(e.getMessage());
		}
	}
}
