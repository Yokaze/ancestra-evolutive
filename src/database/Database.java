package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import core.Server;
import database.data.AccountData;
import database.data.AnimationData;
import database.data.AreaData;
import database.data.AreaSubData;
import database.data.CharacterData;
import database.data.CollectorData;
import database.data.GuildData;
import database.data.GuildMemberData;
import database.data.HouseData;
import database.data.ItemData;
import database.data.ItemSetData;
import database.data.ItemTemplateData;
import database.data.JobData;
import database.data.MapData;
import database.data.MonsterData;
import database.data.MountData;
import database.data.MountparkData;
import database.data.NpcAnswerData;
import database.data.NpcData;
import database.data.NpcQuestionData;
import database.data.NpcTemplateData;
import database.data.ScriptedCellData;
import database.data.SpellData;

public class Database {
	//connection
	private Connection connection;
	//data
	private AccountData accountData;
	private AnimationData animationData;
	private AreaData areaData;
	private AreaSubData areaSubData;
	private CharacterData characterData;
	private CollectorData collectorData;
	private GuildData guildData;
	private GuildMemberData guildMemberData;
	private HouseData houseData;
	private ItemData itemData;
	private ItemSetData itemSetData;
	private ItemTemplateData itemTemplateData;
	private JobData jobData;
	private MapData mapData;
	private MonsterData monsterData;
	private MountData mountData;
	private MountparkData mountparkData;
	private NpcAnswerData npcAnswerData;
	private NpcData npcData;
	private NpcQuestionData npcQuestionData;
	private NpcTemplateData npcTemplateData;
	private ScriptedCellData scriptedCellData;
	private SpellData spellData;
	
	public void initializeData() {
		this.accountData = new AccountData(connection);
		this.animationData = new AnimationData(connection);
		this.areaData = new AreaData(connection);
		this.areaSubData = new AreaSubData(connection);
		this.characterData = new CharacterData(connection);
		this.collectorData = new CollectorData(connection);
		this.guildData = new GuildData(connection);
		this.guildMemberData = new GuildMemberData(connection);
		this.houseData = new HouseData(connection);
		this.itemData = new ItemData(connection);
		this.itemSetData = new ItemSetData(connection);
		this.itemTemplateData = new ItemTemplateData(connection);
		this.jobData = new JobData(connection);
		this.mapData = new MapData(connection);
		this.monsterData = new MonsterData(connection);
		this.npcAnswerData = new NpcAnswerData(connection);
		this.npcData = new NpcData(connection);
		this.npcQuestionData = new NpcQuestionData(connection);
		this.npcTemplateData = new NpcTemplateData(connection);
		this.scriptedCellData = new ScriptedCellData(connection);
		this.spellData = new SpellData(connection);
	}
	
	public boolean initializeConnection() {
		try {
		  this.connection = DriverManager.getConnection("jdbc:mysql://" +
		  Server.config.getHost() + "/" +
				  Server.config.getDatabaseName(), 
				  Server.config.getUser(), 
				  Server.config.getPass());
		  this.connection.setAutoCommit(true);
		  this.initializeData();
		} catch (SQLException e) {
			return false;
		}
		return true;
	}
	
	public AccountData getAccountData() {
		return accountData;
	}
	public AnimationData getAnimationData() {
		return animationData;
	}
	public AreaData getAreaData() {
		return areaData;
	}
	public AreaSubData getAreaSubData() {
		return areaSubData;
	}
	public CharacterData getCharacterData() {
		return characterData;
	}
	public CollectorData getCollectorData() {
		return collectorData;
	}
	public HouseData getHouseData() {
		return houseData;
	}
	public ItemData getItemData() {
		return itemData;
	}
	public ItemSetData getItemSetData() {
		return itemSetData;
	}
	public ItemTemplateData getItemTemplateData() {
		return itemTemplateData;
	}
	public JobData getJobData() {
		return jobData;
	}
	public MapData getMapData() {
		return mapData;
	}
	public MonsterData getMonsterData() {
		return monsterData;
	}
	public MountData getMountData() {
		return mountData;
	}
	public MountparkData getMountparkData() {
		return mountparkData;
	}
	public NpcAnswerData getNpcAnswerData() {
		return npcAnswerData;
	}
	public NpcData getNpcData() {
		return npcData;
	}
	public NpcQuestionData getNpcQuestionData() {
		return npcQuestionData;
	}
	public NpcTemplateData getNpcTemplateData() {
		return npcTemplateData;
	}
	public ScriptedCellData getScriptedCellData() {
		return scriptedCellData;
	}
	public SpellData getSpellData() {
		return spellData;
	}

	public GuildData getGuildData() {
		return guildData;
	}

	public GuildMemberData getGuildMemberData() {
		return guildMemberData;
	}
}
