package realm;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.TimeUnit;

import tool.packetfilter.PacketFilter;

import core.Server;
import core.Log;
import core.Main;

public class RealmServer implements Runnable{

	private ServerSocket _SS;
	private Thread _t;
	
	public static int _totalNonAbo = 0;//Total de connections non abo
	public static int _totalAbo = 0;//Total de connections abo
	public static int _queueID = -1;//Num�ro de la queue
	public static int _subscribe = 1;//File des non abonn�es (0) ou abonn�es (1)
	
	public RealmServer()
	{
		try {
			_SS = new ServerSocket(Server.config.getRealmPort());
			_t = new Thread(this);
			_t.setDaemon(true);
			_t.start();
		} catch (IOException e) {
			Log.addToRealmLog("IOException: "+e.getMessage());
			Main.closeServers();
		}
		
	}

	public void run()
	{	
		PacketFilter filter = new PacketFilter(5, 1, TimeUnit.SECONDS);
		while(Server.config.isRunning()) {
			try {
				if(!filter.authorize(_SS.getInetAddress().getHostAddress()))
					_SS.close();
				else
					new RealmThread(_SS.accept());
			} catch(IOException e) {
				Log.addToRealmLog("IOException: "+e.getMessage());
				try {
					Log.addToRealmLog("Fermeture du serveur de connexion");	
					if(!_SS.isClosed())
						_SS.close();
				} catch(IOException e1) {}
			}
		}
	}
	
	public void kickAll()
	{
		try {
			_SS.close();
		} catch (IOException e) {}
	}
	public Thread getThread()
	{
		return _t;
	}
}
