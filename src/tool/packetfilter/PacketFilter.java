package tool.packetfilter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class PacketFilter {
	
	private int maxConnections, restrictedTime;
	private Map<String, IpInstance> ipInstances = new ConcurrentHashMap<>();
	
	public PacketFilter(int maxConnections, int time, TimeUnit unit) {
		this.maxConnections = maxConnections;
		this.restrictedTime = (int)TimeUnit.MILLISECONDS.convert(time, unit);
	}
	
	public boolean authorize(String ip) {
		IpInstance ipInstance = find(ip);
		
		if (ipInstance.isBanned()) {
			return false;
		} else {
			ipInstance.addConnection();
			
			if (ipInstance.getLastConnection() + this.restrictedTime >= System.currentTimeMillis()) {
				if (ipInstance.getConnections() < this.maxConnections)
					return true;
				else {
					ipInstance.ban();
					return false;
				}
			} else {
				ipInstance.updateLastConnection();
				ipInstance.resetConnections();
			}
			return true;
		}
	}
	
	 private IpInstance find(String ip) {
	    ip = clearIp(ip);
	    
        IpInstance result = ipInstances.get(ip);
        if (result != null) return result;
	    
        result = new IpInstance(ip);
        ipInstances.put(ip, result);
        return result;
	 }
	
	private String clearIp(String ip) {
		return ip.contains(":")?ip.split(":")[0]:ip;
	}
}
