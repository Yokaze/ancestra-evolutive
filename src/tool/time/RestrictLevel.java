package tool.time;

public enum RestrictLevel {
	PLAYER,
	ACCOUNT,
	IP;
}