package game;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import objects.Compte;
import objects.Personnage;
import tool.packetfilter.PacketFilter;

import common.CryptManager;
import common.SQLManager;
import common.SocketManager;
import common.World;

import core.Log;
import core.Main;
import core.Server;

public class GameServer implements Runnable {

	private ServerSocket serverSocket;
	private Thread thread;
	private ArrayList<GameThread> clients = new ArrayList<GameThread>();
	private ArrayList<Compte> waitingClients = new ArrayList<Compte>();
	private long startTime;
	private int maxConnections = 0;
	
	public GameServer(String Ip) {
		ScheduledExecutorService scheduler = World.data.getScheduler();
		
		try {
			scheduler.scheduleWithFixedDelay(new Runnable() {
				public void run() {
					if(!Server.config.isSaving())
						World.data.saveData(-1);
				}
			}, Server.config.getSaveTime(),Server.config.getSaveTime(), TimeUnit.MILLISECONDS);
			
			scheduler.scheduleWithFixedDelay(new Runnable() {
				public void run() {
					SQLManager.LOAD_ACTION();
					Log.addToLog("Les live actions ont ete appliquees");
				}
			}, Server.config.getLoadDelay(),Server.config.getLoadDelay(), TimeUnit.MILLISECONDS);
			
			scheduler.scheduleWithFixedDelay(new Runnable() {
				public void run() {
					World.data.RefreshAllMob();
					Log.addToLog("La recharge des mobs est finie");
				}
			}, Server.config.getReloadMobDelay(),Server.config.getReloadMobDelay(), TimeUnit.MILLISECONDS);
			
			scheduler.scheduleWithFixedDelay(new Runnable() {
				public void run() {
					for(Personnage perso : World.data.getOnlinePersos())  { 
						if (perso.getLastPacketTime() + Server.config.getMaxIdleTime() < System.currentTimeMillis()) {
							if(perso != null && perso.get_compte().getGameThread() != null && perso.isOnline()) {
								Log.addToLog("Kick pour inactiviter de : "+perso.get_name());
								SocketManager.REALM_SEND_MESSAGE(perso.get_compte().getGameThread().get_out(),"01|"); 
								perso.get_compte().getGameThread().closeSocket();
							}
						}
						
					}
				}
			}, 60000,60000, TimeUnit.MILLISECONDS);
			
			serverSocket = new ServerSocket(Server.config.getGamePort());
			
			if(Server.config.isUseIp())
				Server.config.setGameServerIpCrypted(CryptManager.CryptIP(Ip)+CryptManager.CryptPort(Server.config.getGamePort()));
			
			startTime = System.currentTimeMillis();
			thread = new Thread(this);
			thread.start();
		} catch (IOException e) {
			Log.addToLog("IOException: "+e.getMessage());
			System.exit(1);
		}
	}
	
	public void run() {	
		PacketFilter filter = new PacketFilter(5, 1, TimeUnit.SECONDS);
		while(Server.config.isRunning()) {
			try {
				
				Socket socket = serverSocket.accept();
				if(!filter.authorize(socket.getInetAddress().getHostAddress())) {
					socket.close();
				} else {
					clients.add(new GameThread(socket));
					
					if(clients.size() > maxConnections)
						maxConnections = clients.size();
				}
			} catch(IOException e) {
				Log.addToLog("IOException: "+e.getMessage());
				try {
					if(!serverSocket.isClosed())serverSocket.close();
					Main.closeServers();
				} catch(IOException e1) {}
			}
		}
	}
	
	public void kickAll() {
		try {
			serverSocket.close();
		} catch (IOException e) {}
		
		for(GameThread GT : clients) {
			try {
				GT.closeSocket();
			} catch(Exception e){};	
		}
	}
	
	public static String getServerDate() {
		Date actDate = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd");
		String jour = Integer.parseInt(dateFormat.format(actDate))+"";
		
		while(jour.length() <2)
			jour = "0"+jour;
		
		dateFormat = new SimpleDateFormat("MM");
		String mois = (Integer.parseInt(dateFormat.format(actDate))-1)+"";
		
		while(mois.length() <2)
			mois = "0"+mois;
		
		dateFormat = new SimpleDateFormat("yyyy");
		String annee = (Integer.parseInt(dateFormat.format(actDate))-1370)+"";
		return "BD"+annee+"|"+mois+"|"+jour;
	}
	
	public ArrayList<GameThread> getClients() {
		return clients;
	}

	public long getStartTime()
	{
		return startTime;
	}
	
	public int getMaxPlayer()
	{
		return maxConnections;
	}
	
	public int getPlayerNumber()
	{
		return clients.size();
	}
	
	public void delClient(GameThread gameThread) {
		clients.remove(gameThread);
		if(clients.size() > maxConnections)maxConnections = clients.size();
	}

	public synchronized Compte getWaitingCompte(int guid) {
		for (int i = 0; i < waitingClients.size(); i++) 
			if(waitingClients.get(i).get_GUID() == guid)
				return waitingClients.get(i);
		return null;
	}
	
	public synchronized void delWaitingCompte(Compte _compte) {
		waitingClients.remove(_compte);
	}
	
	public synchronized void addWaitingCompte(Compte _compte) {
		waitingClients.add(_compte);
	}
	
	public static String getServerTime() {
		Date actDate = new Date();
		return "BT"+(actDate.getTime()+3600000);
	}

	public Thread getThread() {
		return thread;
	}
}
